var gulp = require("gulp"),
	pathPrefix = "",
	gutil = require("gulp-util"),
	sass = require("gulp-sass"),
	sourcemaps = require("gulp-sourcemaps"),
	autoprefixer = require("gulp-autoprefixer"),

	scssPath = pathPrefix + "./assets/scss",
	outputPath = pathPrefix + "./assets/css/",
	filesToWatch = [scssPath + "/**/*.scss"],
	fileToCompile = scssPath + '/main.scss';




gulp.task('process-sass', function() {
	return gulp.src(fileToCompile)
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'compact'}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		//.pipe(autoprefixer())
		.pipe(gulp.dest(outputPath));
});

gulp.task('watch', function() {
	gulp.watch(filesToWatch, gulp.series('process-sass'));
});

//for dev/local
gulp.task('default', gulp.series('process-sass', 'watch'));